## How to compile and run:

```
g++ -std=c++17 -Wall -Wextra -pedantic main.cpp && rlwrap ./a.out
```

## Tested with

1. Apple clang version 12.0.0 (clang-1200.0.32.29), Target: x86_64-apple-darwin19.6.0
2. Linux x86-64 gcc 11.2
