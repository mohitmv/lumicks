#include <iostream>

#include <string>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <algorithm>

struct Command {
  std::string_view main_command;
  std::vector<std::string_view> params;
};

struct Response {
  bool success;
  std::vector<std::string> params;
  Response(bool success): success(success) {}
  Response(bool success, std::string&& param): success(success) {
    params.push_back(std::move(param));
  }
};

// "ABC"  -> {"ABC", params={}}
// "ABC|DEF"  -> {"ABC", params={"DEF"}}
// "ABC|DEF|GHI" -> {"ABC", params={"DEF", "GHI"}}
Command ParseCommand(const std::string& cmd_str) {
  Command cmd;
  size_t buffer_start_at = 0;
  size_t len = cmd_str.size();
  for (size_t i = 0; i <= len; i++) {
    if (i == len or cmd_str[i] == '|') {
      if (buffer_start_at == 0) {
        cmd.main_command = std::string_view(cmd_str.data(), i);
      } else {
        cmd.params.push_back(std::string_view(
          cmd_str.data() + buffer_start_at, i - buffer_start_at));
      }
      buffer_start_at = i + 1;
    }
  }
  return cmd;
}

time_t TimeNowMicroSeconds() {
  using namespace std::chrono;  // NOLINT
  auto epoch_time = system_clock::now().time_since_epoch();
  return duration_cast<microseconds>(epoch_time).count();
}

class Device;

using HandlerFuncType = Response (Device::*)(const Command& cmd);
using HandlerFuncMap = std::unordered_map<std::string_view, HandlerFuncType>;

class Device {
 public:
  std::string HandleCommand(std::string&& cmd_str);

 private:
  Response Start(const Command& cmd);
  Response Stop(const Command& cmd);
  Response IsStarted(const Command& cmd);
  Response KeepAlive(const Command& cmd);
  Response GetPower(const Command& cmd);
  Response SetPower(const Command& cmd);
  Response EnableSillyMode(const Command& cmd);
  Response DisableSillyMode(const Command& cmd);
  Response Exit(const Command& cmd);

 private:
  // Singleton instance of the map(command string -> handler function)
  // Return reference of singleton.
  // We could have maintained this map as a static member, but that would have
  // suffered from the "initialization order fiasco".
  HandlerFuncMap& CmdHandlerMap();
  // Returns true iff the emission is currently ON.
  bool IsStartedInternal() const {
    return IsStartedInternal(TimeNowMicroSeconds());
  }
  bool IsStartedInternal(time_t time_now_us) const {
    constexpr time_t auto_stop_delay = 5L * 1000L * 1000L; // micro seconds.
    return recent_alive_timestamp_us + auto_stop_delay >= time_now_us;
  }
  // The timestamp of the most recent keep-alive command.
  // STR command have the effect of keep-alive signal as well implicitly.
  // Timestamp in micro seconds since epoch.
  time_t recent_alive_timestamp_us = 0;
  int laser_power = 1;
  bool is_silly_mode = false;
};


HandlerFuncMap& Device::CmdHandlerMap() {
  static HandlerFuncMap cmd_handler_map = {
    {"STR", &Device::Start},
    {"STP", &Device::Stop},
    {"ST?", &Device::IsStarted},
    {"KAL", &Device::KeepAlive},
    {"PW?", &Device::GetPower},
    {"PW=", &Device::SetPower},
    {"ESM", &Device::EnableSillyMode},
    {"DSM", &Device::DisableSillyMode},
    {"EXIT", &Device::Exit},
  };
  return cmd_handler_map;
}

std::string Device::HandleCommand(std::string&& cmd_str) {
  auto& cmd_handler_map = CmdHandlerMap();
  if (is_silly_mode) {
    std::reverse(cmd_str.begin(), cmd_str.end());
  }
  auto cmd = ParseCommand(cmd_str);
  auto&& handler_it = cmd_handler_map.find(cmd.main_command);
  if (handler_it == cmd_handler_map.end()) {
    return "UK!";
  }
  HandlerFuncType& handler = handler_it->second;
  Response response = (this->*handler)(cmd);
  if (not response.success) {
    return std::string(cmd.main_command) + "!";
  }
  std::string output(cmd.main_command);
  for (auto& param: response.params) {
    output += '|';
    output += param;
  }
  output += '#';
  return output;
}

Response Device::Start(const Command&) {
  auto time_now = TimeNowMicroSeconds();
  if (IsStartedInternal(time_now)) return {false};
  recent_alive_timestamp_us = time_now;
  return {true};
}

Response Device::Stop(const Command&) {
  auto time_now = TimeNowMicroSeconds();
  if (not IsStartedInternal(time_now)) return {false};
  recent_alive_timestamp_us = 0;
  return {true};
}

Response Device::IsStarted(const Command&) {
  return Response(true, std::to_string(IsStartedInternal() ? 1 : 0));
}

Response Device::KeepAlive(const Command&) {
  auto time_now = TimeNowMicroSeconds();
  if (not IsStartedInternal(time_now)) return {false};
  recent_alive_timestamp_us = time_now;
  return {true};
}

Response Device::GetPower(const Command&) {
  if (not IsStartedInternal()) return Response(true, "0");
  return Response(true, std::to_string(laser_power));
}

// Precondition(output != nullptr)
// Precondition(input should be a non-negative number)
// Return false in case of error.
// In case of error, @output will not be modified.
bool StringViewToInt(std::string_view input, int* output) {
  int tmp_output = 0;
  for (auto c : input) {
    if (not ('0' <= c && c <= '9')) return false;
    tmp_output *= 10;
    tmp_output += c - '0';
  }
  *output = tmp_output;
  return true;
}

Response Device::SetPower(const Command& cmd) {
  if (not IsStartedInternal() || cmd.params.size() != 1) return {false};
  int power = 0;
  if (not StringViewToInt(cmd.params[0], &power)) return {false};
  if (not (1 <= power && power <= 100)) return {false};
  laser_power = power;
  return {true};
}

Response Device::EnableSillyMode(const Command&) {
  is_silly_mode = true;
  return {true};
}

Response Device::DisableSillyMode(const Command&) {
  is_silly_mode = false;
  return {true};
}

Response Device::Exit(const Command&) {
  std::cout << "Exiting..." << std::endl;
  std::exit(0);
  return {true};
}

int main() {
  Device device;
  while (true) {
    std::cout << "> " << std::flush;
    std::string cmd_str;
    std::getline(std::cin, cmd_str);
    std::cout << device.HandleCommand(std::move(cmd_str)) << std::endl;
  }
}
